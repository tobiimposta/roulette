import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import model.Model;


public class Main extends Application {

    private Timer timer;


    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Willkommen zum Roulette");

        Model model = new Model();


        Button zahl0 = new Button();
        zahl0.setText("0");
        zahl0.setPrefSize(400, 35);
        zahl0.setLayoutX(500);
        zahl0.setLayoutY(100);
        zahl0.setOnMouseClicked(event -> model.wette(0));

        Button zahl1 = new Button();
        zahl1.setText("1");
        zahl1.setPrefSize(100, 35);
        zahl1.setLayoutX(500);
        zahl1.setLayoutY(135);
        zahl1.setOnMouseClicked(event -> model.wette(1));

        Button zahl2 = new Button();
        zahl2.setText("2");
        zahl2.setPrefSize(100, 35);
        zahl2.setLayoutX(600);
        zahl2.setLayoutY(135);
        zahl2.setOnMouseClicked(event -> model.wette(2));

        Button zahl3 = new Button();
        zahl3.setText("3");
        zahl3.setPrefSize(100, 35);
        zahl3.setLayoutX(700);
        zahl3.setLayoutY(135);
        zahl3.setOnMouseClicked(event -> model.wette(3));

        Button zahl4 = new Button();
        zahl4.setText("4");
        zahl4.setPrefSize(100, 35);
        zahl4.setLayoutX(800);
        zahl4.setLayoutY(135);
        zahl4.setOnMouseClicked(event -> model.wette(4));

        Button zahl5 = new Button();
        zahl5.setText("5");
        zahl5.setPrefSize(100, 35);
        zahl5.setLayoutX(500);
        zahl5.setLayoutY(170);
        zahl5.setOnMouseClicked(event -> model.wette(5));

        Button zahl6 = new Button();
        zahl6.setText("6");
        zahl6.setPrefSize(100, 35);
        zahl6.setLayoutX(600);
        zahl6.setLayoutY(170);
        zahl6.setOnMouseClicked(event -> model.wette(6));

        Button zahl7 = new Button();
        zahl7.setText("7");
        zahl7.setPrefSize(100, 35);
        zahl7.setLayoutX(700);
        zahl7.setLayoutY(170);
        zahl7.setOnMouseClicked(event -> model.wette(7));

        Button zahl8 = new Button();
        zahl8.setText("8");
        zahl8.setPrefSize(100, 35);
        zahl8.setLayoutX(800);
        zahl8.setLayoutY(170);
        zahl8.setOnMouseClicked(event -> model.wette(8));

        Button zahl9 = new Button();
        zahl9.setText("9");
        zahl9.setPrefSize(100, 35);
        zahl9.setLayoutX(500);
        zahl9.setLayoutY(205);
        zahl9.setOnMouseClicked(event -> model.wette(9));

        Button zahl10 = new Button();
        zahl10.setText("10");
        zahl10.setPrefSize(100, 35);
        zahl10.setLayoutX(600);
        zahl10.setLayoutY(205);
        zahl10.setOnMouseClicked(event -> model.wette(10));

        Button zahl11 = new Button();
        zahl11.setText("11");
        zahl11.setPrefSize(100, 35);
        zahl11.setLayoutX(700);
        zahl11.setLayoutY(205);
        zahl11.setOnMouseClicked(event -> model.wette(11));

        Button zahl12 = new Button();
        zahl12.setText("12");
        zahl12.setPrefSize(100, 35);
        zahl12.setLayoutX(800);
        zahl12.setLayoutY(205);
        zahl12.setOnMouseClicked(event -> model.wette(12));

        Button zahl13 = new Button();
        zahl13.setText("13");
        zahl13.setPrefSize(100, 35);
        zahl13.setLayoutX(500);
        zahl13.setLayoutY(240);
        zahl13.setOnMouseClicked(event -> model.wette(13));

        Button zahl14 = new Button();
        zahl14.setText("14");
        zahl14.setPrefSize(100, 35);
        zahl14.setLayoutX(600);
        zahl14.setLayoutY(240);
        zahl14.setOnMouseClicked(event -> model.wette(14));

        Button zahl15 = new Button();
        zahl15.setText("15");
        zahl15.setPrefSize(100, 35);
        zahl15.setLayoutX(700);
        zahl15.setLayoutY(240);
        zahl15.setOnMouseClicked(event -> model.wette(15));

        Button zahl16 = new Button();
        zahl16.setText("16");
        zahl16.setPrefSize(100, 35);
        zahl16.setLayoutX(800);
        zahl16.setLayoutY(240);
        zahl16.setOnMouseClicked(event -> model.wette(16));

        Button zahl17 = new Button();
        zahl17.setText("17");
        zahl17.setPrefSize(100, 35);
        zahl17.setLayoutX(500);
        zahl17.setLayoutY(275);
        zahl17.setOnMouseClicked(event -> model.wette(17));

        Button zahl18 = new Button();
        zahl18.setText("18");
        zahl18.setPrefSize(100, 35);
        zahl18.setLayoutX(600);
        zahl18.setLayoutY(275);
        zahl18.setOnMouseClicked(event -> model.wette(18));

        Button zahl19 = new Button();
        zahl19.setText("19");
        zahl19.setPrefSize(100, 35);
        zahl19.setLayoutX(700);
        zahl19.setLayoutY(275);
        zahl19.setOnMouseClicked(event -> model.wette(19));

        Button zahl20 = new Button();
        zahl20.setText("20");
        zahl20.setPrefSize(100, 35);
        zahl20.setLayoutX(800);
        zahl20.setLayoutY(275);
        zahl20.setOnMouseClicked(event -> model.wette(20));

        Button zahl21 = new Button();
        zahl21.setText("21");
        zahl21.setPrefSize(100, 35);
        zahl21.setLayoutX(500);
        zahl21.setLayoutY(310);
        zahl21.setOnMouseClicked(event -> model.wette(21));

        Button zahl22 = new Button();
        zahl22.setText("22");
        zahl22.setPrefSize(100, 35);
        zahl22.setLayoutX(600);
        zahl22.setLayoutY(310);
        zahl22.setOnMouseClicked(event -> model.wette(22));

        Button zahl23 = new Button();
        zahl23.setText("23");
        zahl23.setPrefSize(100, 35);
        zahl23.setLayoutX(700);
        zahl23.setLayoutY(310);
        zahl23.setOnMouseClicked(event -> model.wette(23));

        Button zahl24 = new Button();
        zahl24.setText("24");
        zahl24.setPrefSize(100, 35);
        zahl24.setLayoutX(800);
        zahl24.setLayoutY(310);
        zahl24.setOnMouseClicked(event -> model.wette(24));

        Button zahl25 = new Button();
        zahl25.setText("25");
        zahl25.setPrefSize(100, 35);
        zahl25.setLayoutX(500);
        zahl25.setLayoutY(345);
        zahl25.setOnMouseClicked(event -> model.wette(25));

        Button zahl26 = new Button();
        zahl26.setText("26");
        zahl26.setPrefSize(100, 35);
        zahl26.setLayoutX(600);
        zahl26.setLayoutY(345);
        zahl26.setOnMouseClicked(event -> model.wette(26));

        Button zahl27 = new Button();
        zahl27.setText("27");
        zahl27.setPrefSize(100, 35);
        zahl27.setLayoutX(700);
        zahl27.setLayoutY(345);
        zahl27.setOnMouseClicked(event -> model.wette(27));

        Button zahl28 = new Button();
        zahl28.setText("28");
        zahl28.setPrefSize(100, 35);
        zahl28.setLayoutX(800);
        zahl28.setLayoutY(345);
        zahl28.setOnMouseClicked(event -> model.wette(28));

        Button zahl29 = new Button();
        zahl29.setText("29");
        zahl29.setPrefSize(100, 35);
        zahl29.setLayoutX(500);
        zahl29.setLayoutY(380);
        zahl29.setOnMouseClicked(event -> model.wette(29));

        Button zahl30 = new Button();
        zahl30.setText("30");
        zahl30.setPrefSize(100, 35);
        zahl30.setLayoutX(600);
        zahl30.setLayoutY(380);
        zahl30.setOnMouseClicked(event -> model.wette(30));

        Button zahl31 = new Button();
        zahl31.setText("31");
        zahl31.setPrefSize(100, 35);
        zahl31.setLayoutX(700);
        zahl31.setLayoutY(380);
        zahl31.setOnMouseClicked(event -> model.wette(31));

        Button zahl32 = new Button();
        zahl32.setText("32");
        zahl32.setPrefSize(100, 35);
        zahl32.setLayoutX(800);
        zahl32.setLayoutY(380);
        zahl32.setOnMouseClicked(event -> model.wette(32));

        Button zahl33 = new Button();
        zahl33.setText("33");
        zahl33.setPrefSize(100, 35);
        zahl33.setLayoutX(500);
        zahl33.setLayoutY(415);
        zahl33.setOnMouseClicked(event -> model.wette(33));

        Button zahl34 = new Button();
        zahl34.setText("34");
        zahl34.setPrefSize(100, 35);
        zahl34.setLayoutX(600);
        zahl34.setLayoutY(415);
        zahl34.setOnMouseClicked(event -> model.wette(34));

        Button zahl35 = new Button();
        zahl35.setText("35");
        zahl35.setPrefSize(100, 35);
        zahl35.setLayoutX(700);
        zahl35.setLayoutY(415);
        zahl35.setOnMouseClicked(event -> model.wette(35));

        Button zahl36 = new Button();
        zahl36.setText("36");
        zahl36.setPrefSize(100, 35);
        zahl36.setLayoutX(800);
        zahl36.setLayoutY(415);
        zahl36.setOnMouseClicked(event -> model.wette(36));


        Button farbeRot = new Button();
        farbeRot.setText("ROT");
        farbeRot.setPrefSize(200, 70);
        farbeRot.setLayoutX(500);
        farbeRot.setLayoutY(450);
        farbeRot.setOnMouseClicked(event -> model.wetteFarbe(0));

        Button farbeSchwarz = new Button();
        farbeSchwarz.setText("SCHWARZ");
        farbeSchwarz.setPrefSize(200, 70);
        farbeSchwarz.setLayoutX(700);
        farbeSchwarz.setLayoutY(450);
        farbeSchwarz.setOnMouseClicked(event -> model.wetteFarbe(1));


        Button einsatz10 = new Button();
        einsatz10.setText("10");
        einsatz10.setPrefSize(50, 50);
        einsatz10.setLayoutX(110);
        einsatz10.setLayoutY(325);
        einsatz10.setOnMouseClicked(event -> model.setSpielEinsatz(10));


        Button einsatz20 = new Button();
        einsatz20.setText("20");
        einsatz20.setPrefSize(50, 50);
        einsatz20.setLayoutX(160);
        einsatz20.setLayoutY(325);
        einsatz20.setOnMouseClicked(event -> model.setSpielEinsatz(20));


        Button einsatz50 = new Button();
        einsatz50.setText("50");
        einsatz50.setPrefSize(50, 50);
        einsatz50.setLayoutX(210);
        einsatz50.setLayoutY(325);
        einsatz50.setOnMouseClicked(event -> model.setSpielEinsatz(50));


        Button einsatz100 = new Button();
        einsatz100.setText("100");
        einsatz100.setPrefSize(50, 50);
        einsatz100.setLayoutX(260);
        einsatz100.setLayoutY(325);
        einsatz100.setOnMouseClicked(event -> model.setSpielEinsatz(100));


        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);
        Group group = new Group();
        group.getChildren().add(canvas);
        group.getChildren().add(farbeRot);
        group.getChildren().add(farbeSchwarz);
        group.getChildren().add(zahl0);
        group.getChildren().add(zahl1);
        group.getChildren().add(zahl2);
        group.getChildren().add(zahl3);
        group.getChildren().add(zahl4);
        group.getChildren().add(zahl5);
        group.getChildren().add(zahl6);
        group.getChildren().add(zahl7);
        group.getChildren().add(zahl8);
        group.getChildren().add(zahl9);
        group.getChildren().add(zahl10);
        group.getChildren().add(zahl11);
        group.getChildren().add(zahl12);
        group.getChildren().add(zahl13);
        group.getChildren().add(zahl14);
        group.getChildren().add(zahl15);
        group.getChildren().add(zahl16);
        group.getChildren().add(zahl17);
        group.getChildren().add(zahl18);
        group.getChildren().add(zahl19);
        group.getChildren().add(zahl20);
        group.getChildren().add(zahl21);
        group.getChildren().add(zahl22);
        group.getChildren().add(zahl23);
        group.getChildren().add(zahl24);
        group.getChildren().add(zahl25);
        group.getChildren().add(zahl26);
        group.getChildren().add(zahl27);
        group.getChildren().add(zahl28);
        group.getChildren().add(zahl29);
        group.getChildren().add(zahl30);
        group.getChildren().add(zahl31);
        group.getChildren().add(zahl32);
        group.getChildren().add(zahl33);
        group.getChildren().add(zahl34);
        group.getChildren().add(zahl35);
        group.getChildren().add(zahl36);
        group.getChildren().add(einsatz10);
        group.getChildren().add(einsatz20);
        group.getChildren().add(einsatz50);
        group.getChildren().add(einsatz100);

        Scene scene = new Scene(group);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);

        primaryStage.show();


        GraphicsContext gc = canvas.getGraphicsContext2D();

        Graphics graphics = new Graphics(model, gc);

        timer = new Timer(model, graphics);
        timer.start();


    }

    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }
}

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Model;


public class Graphics {
    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    Image wheel = new Image("roulettewheel.jpg");

    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.drawImage(wheel, 0, 0, Model.WIDTH, Model.HEIGHT);
        gc.setFill(Color.WHITE);
        gc.setFont(Font.font(22));
        gc.fillText("Wie viel möchten Sie setzen?", 75, 315);
        gc.fillText("Auf was möchten Sie setzen?", 550, 90);
        gc.setFont(Font.font(20));
        gc.fillText("Kontostand: " + model.getNeuerKontostand(), 50, 30);

        if (model.checkEinsatz(model.getSpielEinsatz()) == false && model.getNeuerKontostand() > 0) {
            gc.setFill(Color.ORANGERED);
            gc.fillText("Sie haben zu wenig Geld auf dem Konto!", 50, 90);
            gc.fillText("Wählen Sie einen anderen Betrag", 50, 110);
        }
        if (model.getNeuerKontostand() == 0) {
            gc.fillText("Sie haben kein Geld mehr auf dem Konto. Gehen Sie nach Hause!", 400, 580);
        }

        if (model.isGewonnenFarbe() == 1) {
            gc.setFill(Color.WHITE);
            gc.fillText("Sie haben gewonnen", 50, 50);


        }
        if (model.isGewonnenZahl() == 1) {
            gc.setFill(Color.WHITE);
            gc.fillText("Sie haben gewonnen", 50, 50);


        }

        if (model.isGewonnenFarbe() == 2) {
            gc.setFill(Color.WHITE);
            gc.fillText("Sie haben leider nicht gewonnen", 50, 50);
            gc.fillText("Es kam leider eine andere Farbe", 50, 70);


        }

        if (model.isGewonnenZahl() == 2) {
            gc.setFill(Color.WHITE);
            gc.fillText("Sie haben leider nicht gewonnen", 50, 50);
            gc.fillText("Es kam leider :" + model.getGedrehteZahl(), 50, 70);


        }
    }
}

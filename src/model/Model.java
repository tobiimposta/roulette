package model;

import java.util.Random;

public class Model {


    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;

    private int neuerKontostand;
    private int anfangsKontostand = 300;
    private int spielEinsatz;

    public int getSpielEinsatz() {
        return spielEinsatz;
    }


    public int getNeuerKontostand() {
        return neuerKontostand;
    }

    public int getGedrehteZahl() {
        return gedrehteZahl;
    }


    public int isGewonnenFarbe() {
        return gewonnenFarbe;
    }

    public int isGewonnenZahl() {
        return gewonnenZahl;
    }


    private int gewonnenZahl = 0;
    private int gewonnenFarbe = 0;


    public Model() {
        this.neuerKontostand = this.anfangsKontostand;
    }

    int gedrehteZahl;

    public void wette(int i) {
        if (checkEinsatz(spielEinsatz) == true) {
            gewonnenFarbe = 0;
            gewonnenZahl = 0;
            Random random = new Random();
            gedrehteZahl = random.nextInt(37);
            if (i == gedrehteZahl) {
                gewonnenZahl = 1;
            } else {
                gewonnenZahl = 2;
            }

            aktualisiereKontostandZahl();
            setSpielEinsatz(0);
        }
    }

    int gedrehteFarbe;

    public void wetteFarbe(int farbe) {
        if (checkEinsatz(spielEinsatz) == true) {
            gewonnenFarbe = 0;
            gewonnenZahl = 0;
            Random random = new Random();
            gedrehteFarbe = random.nextInt(2);
            if (farbe == gedrehteFarbe) {
                gewonnenFarbe = 1;
            } else {
                gewonnenFarbe = 2;
            }
            aktualisiereKontostandFarbe();
            setSpielEinsatz(0);
        }
    }


    public int aktualisiereKontostandZahl() {
        if (gewonnenZahl == 1) {
            neuerKontostand += spielEinsatz * 35;

        }
        if (gewonnenZahl == 2) {
            neuerKontostand -= spielEinsatz;
        }
        return neuerKontostand;
    }

    public int aktualisiereKontostandFarbe() {
        if (gewonnenFarbe == 1) {
            neuerKontostand += spielEinsatz;

        }
        if (gewonnenFarbe == 2) {
            neuerKontostand -= spielEinsatz;
        }
        return neuerKontostand;
    }

    public boolean checkEinsatz(int spielEinsatz) {
        neuerKontostand = getNeuerKontostand();
        if (spielEinsatz <= neuerKontostand) {
            return true;
        } else {
            return false;
        }
    }

    public void setSpielEinsatz(int spielEinsatz) {
        this.spielEinsatz = spielEinsatz;
        checkEinsatz(spielEinsatz);
    }


}





